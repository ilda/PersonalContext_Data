# Experiment Data
## Personal+Context Navigation: Combining AR and Shared Displays in Network Path-following

This repository contains :
- Data from the 2 experiments in folder "data":
	- = Path Selection:
		Completion time (tct-expe-1.csv)
		Questionnaire responses (post-questionnaire-expe-1.csv)

	- = Path Tracing:
		Completion time (tct-expe-2.csv)
		Questionnaire responses (post-questionnaire-expe-2.csv)

- Screenshot of the paths used for the experiment in the folder "pathScreenshot"

Notes: 
- In questionnaire data M: MagneticArea, Me: MagneticElastic, S: SlidingRing, Se: SlidingElastic, B: Baseline
- The completion time files include the training trials as indicated by the Training column
